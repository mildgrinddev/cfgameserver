//
//  CFHttpReportLU.cpp
//  SocketGameServer
//
//  Created by apple on 2020/5/6.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include "CFHttpReportLU.hpp"

CFHttpReportLU::CFHttpReportLU(string _uuid, int _port, int _level, vector<CFCmdInfo>& _httpRequestCmdList, mutex& _mutex):uuid(_uuid), port(_port), level(_level),
                                                                                        httpRequestCmdList(_httpRequestCmdList), gmutex(_mutex)  {
}

void CFHttpReportLU::run() {
    
    CFTickable::reset();
    
    reportAccumulate = 0;
        
    while (true) {
   
        steady_clock::time_point now = steady_clock::now();
        duration<double> time_span = duration_cast<duration<double> >(now - start);
        
        reportAccumulate += time_span.count();
        
        if (reportAccumulate > HTTPREQUEST_REPORT_TIME) {
            
            ////////////////////////
            // Report
            ////////////////////////
            system("curl -o ip.txt https://checkip.amazonaws.com;cat ip.txt;");
            cout << "========= get ip done ========" << endl;

            // open a file in write mode.
            ifstream outfile;
            outfile.open("ip.txt");
            string ip;
            std::getline(outfile, ip);

            // write inputted data into the file.
            cout << ip << endl;

            // close the opened file.
            outfile.close();

            // output
            cout << "ip_port:" << port << endl;

            // if ip_line is empty; return
            if (ip.length() == 0) {
              cout << "ip is not ready; then return;" << endl;
              return;
            }
            cout << "Report IP Port" << endl;
            string allpar = "uuid=";
            allpar = allpar.append(uuid);
            allpar = allpar.append("&ip=");
            allpar = allpar.append(ip);
            allpar = allpar.append("&port=");
            allpar = allpar.append(to_string(port));
            allpar = allpar.append("&level=");
            allpar = allpar.append(to_string(level));
            string command = "curl -X POST -d \"";
            command = command.append(allpar.c_str());
            command = command.append("\" ");
            command = command.append(DISTRIBUT_SERVER_REPORT);
            cout << command << endl;
            system(command.c_str());
            
            /////////////////////////////////////
            // Digest all http command in queue
            /////////////////////////////////////
            gmutex.lock();
            vector<CFCmdInfo>::iterator itr;
            for (itr = httpRequestCmdList.begin(); itr != httpRequestCmdList.end(); itr++) {
                CFCmdInfo cmdInfo = (*itr);
                if (cmdInfo.cmd == CFCmd_GameStatistic) {
                    
                    cout << "send statistic" << endl;
                    
                } else if (cmdInfo.cmd == CFCmd_GameOver) {
                    
                    string allpar2 = "";
                    allpar2 = allpar2.append("ip=");
                    allpar2 = allpar2.append(ip);
                    allpar2 = allpar2.append("&port=");
                    allpar2 = allpar2.append(to_string(port));
                    allpar2 = allpar2.append("&level=");
                    allpar2 = allpar2.append(to_string(level));
                    string command2 = "curl -X POST -d \"";
                    command2 = command2.append(allpar2.c_str());
                    command2 = command2.append("\" ");
                    command2 = command2.append(DISTRIBUT_SERVER_GAMEOVER);
                    cout << command2 << endl;
                    system(command2.c_str());
                }
            }
            httpRequestCmdList.clear();
            gmutex.unlock();
            reportAccumulate = 0;
        }
    }
}
