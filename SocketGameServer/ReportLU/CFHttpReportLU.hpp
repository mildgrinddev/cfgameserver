//
//  CFHttpReportLU.hpp
//  SocketGameServer
//
//  Created by apple on 2020/5/6.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFHttpReportLU_hpp
#define CFHttpReportLU_hpp

#include <stdio.h>

class CFHttpReportLU : CFTickable {
    
    double reportAccumulate;

    string uuid;
    
    int port;
    
    int level;
    
    vector<CFCmdInfo>& httpRequestCmdList;
    
    mutex& gmutex;
        
public:
    CFHttpReportLU(string _uuid, int _port, int _level, vector<CFCmdInfo>& _httpRequestCmdList, mutex& _mutex);
    
public:
    void run() override;
};
#endif
