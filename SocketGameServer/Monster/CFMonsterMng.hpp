//
//  CFMonsterMng.hpp
//  SocketGameServer
//
//  Created by apple on 2020/4/22.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFMonsterMng_hpp
#define CFMonsterMng_hpp

class CFMonsterMng : public CFTickable {
  
public:
    CFMonsterMng();
    
    void run() override;
    
};
#endif
