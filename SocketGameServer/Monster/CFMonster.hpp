//
//  CFMonster.hpp
//  SocketGameServer
//
//  Created by apple on 2020/4/21.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFMonster_hpp
#define CFMonster_hpp

class CFMonster : public CFTickable {
  
public:
    CFMonster();
    
public:
    void run() override;
};
#endif
