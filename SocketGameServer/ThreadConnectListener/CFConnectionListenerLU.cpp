//
//  CFConnectionListenerLU.cpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include "CFConnectionListenerLU.hpp"

CFConnectionListenerLU::CFConnectionListenerLU(map<int, string>& _clientMap, int _port):clientMap(_clientMap), port(_port) {
    
    reset();
}

void CFConnectionListenerLU::reset() {

    CFTickable::reset();
    
    playTotal = 0;

    accumulate = 0;
}

void CFConnectionListenerLU::run() {
    
    int sockfd = 0, forClientSockfd = 0;
    sockfd = socket(AF_INET , SOCK_STREAM , 0);
    if (sockfd == -1) {

        cout << "FAIL TO CREATE A SERVER SOCKET." << endl;
    }
    struct sockaddr_in serverInfo, clientInfo;
    socklen_t addrlen = sizeof(clientInfo);
    bzero(&serverInfo, sizeof(serverInfo));
    serverInfo.sin_family = PF_INET;
    serverInfo.sin_addr.s_addr = INADDR_ANY;
    serverInfo.sin_port = htons(port);
    ::bind(sockfd, (struct sockaddr *)&serverInfo, sizeof(serverInfo));
    listen(sockfd, 5);
    cout << "Start listen" << port << endl;

    // Polling to accept client since we use a non-blocking description
    mutex gmutex;
    while (1) {
        
        gmutex.lock();
        if (clientMap.size() == CLIENT_ACCEPTED_LIMIT) {
            gmutex.unlock();
            continue;
        }
        
        forClientSockfd = accept(sockfd,(struct sockaddr*) &clientInfo, &addrlen);
        fcntl(forClientSockfd, F_SETFL, O_NONBLOCK);
        cout << "New connection" << endl;
        clientMap[forClientSockfd] = string("");
        gmutex.unlock();
    }
}
