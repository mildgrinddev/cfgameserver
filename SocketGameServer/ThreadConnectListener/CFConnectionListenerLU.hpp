//
//  CFConnectionListenerLU.hpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFConnectionListenerLU_hpp
#define CFConnectionListenerLU_hpp

class CFConnectionListenerLU : CFTickable {
    
    map<int, string>& clientMap;
    
    int port;
    
public:
    CFConnectionListenerLU(map<int, string>& _clientMap, int _port);
    
protected:
    void reset() override;

public:
    void run() override;
};
#endif
