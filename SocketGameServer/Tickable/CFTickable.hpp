//
//  ITickable.h
//  SocketGameServer
//
//  Created by apple on 2020/4/22.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef ITickable_hpp
#define ITickable_hpp

#include <atomic>
#include <fcntl.h>
#include <pthread.h>
#include <chrono>
#include <ctime>

using namespace std::chrono;

class CFTickable {
    
protected:
    steady_clock::time_point start;
    double accumulate;
    double playTotal;
    
public:
    CFTickable();
    
    virtual void reset();
    
    virtual void run() = 0;
};

#endif
