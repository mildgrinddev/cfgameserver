//
//  Tickable.cpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include "CFTickable.hpp"
#include <iostream>

using namespace std;

CFTickable::CFTickable() {
    
}

void CFTickable::reset() {

    start = steady_clock::now();
    accumulate = 0;
    playTotal = 0;
}
