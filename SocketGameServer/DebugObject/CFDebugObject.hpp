//
//  CFDebugObject.hpp
//  SocketGameServer
//
//  Created by apple on 2020/4/21.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFDebugObject_hpp
#define CFDebugObject_hpp

class CFDebugObject : protected CFTickable {
    
public:
    void run() override;
};
#endif
