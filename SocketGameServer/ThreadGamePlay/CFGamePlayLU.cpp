//
//  CFGamePlayLU.cpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include "CFGamePlayLU.hpp"
static const int debuginfo_delay = 25;

CFGamePlayLU::CFGamePlayLU(map<int, string>& _clientMap, map<string, CFClientInfo>& _clientInfo,
                           vector<CFCmdInfo>& _broadCastCmdList,
                           vector<CFCmdInfo>& _clientCmdList,
                           vector<CFCmdInfo>& _httpReportCmdList, mutex& _mutex):clientMap(_clientMap), clientInfo(_clientInfo),broadCastCmdList(_broadCastCmdList),
httpReportCmdList(_httpReportCmdList), clientCmdList(_clientCmdList), gmutex(_mutex) {
    
    reset();
}

void CFGamePlayLU::reset() {

    CFTickable::reset();
    
    global_GameState = CFGamePlayState_SystemIdle;
    
    playTotal = 0;
    
    accumulate = 0;
    
    sysIdleAccumulate = 0;
    
    prepareAccumulate = 0;
    
    countdownAccumulate = 0;
    
    heartBeatAccumulate = 0;
    
    playAccumulate = 0;
    
    endAccumulate = 0;
    
    start = steady_clock::now();
    
    global_Pos[0] = global_InitPos[0];
    global_Pos[1] = global_InitPos[1];
    global_TargetPos[0] = global_InitTargetPos[0];
    global_TargetPos[1] = global_InitTargetPos[1];
    
}

void CFGamePlayLU::run() {

    static double sysidle_c = 0;
    static double idle_c = 0;
    static double prepare_c = 0;
    static double countdown_c = 0;
    static double playing_c = 0;
    static double ended_c = 0;
    
    while (true) {
                
        steady_clock::time_point now = steady_clock::now();
        duration<double> time_span = duration_cast<duration<double> >(now - start);
        start = now;
        
        // Accumulate
        accumulate += time_span.count();
        
        // Total play time
        playTotal += time_span.count();
                
        // Roughly tick 1 / 60 second
        if (accumulate > GAME_STEP_TIME_MSS) {
                   
            // Heart Beat --------
            if (global_GameState != CFGamePlayState_SystemIdle && global_GameState != CFGamePlayState_Idle) {
            
                heartBeatAccumulate += accumulate;
                
                //Heart Beat
                if (heartBeatAccumulate > HEARTBEAT_TIME) {
                           
                    heartBeatAccumulate = 0;
                                
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd::CFCmd_HeartBeat;
                    gmutex.lock();
                    broadCastCmdList.push_back(cmdInfo);
                    gmutex.unlock();
                    
                    cout << "Heart Beat ..." << sysIdleAccumulate << endl;
                }
            }

            // Logic --------
            if (global_GameState == CFGamePlayState_SystemIdle) {
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                sysidle_c += time_span.count();
                if (sysidle_c > debuginfo_delay) {
                    cout << "System Idel..." << sysIdleAccumulate << endl;
                    sysidle_c = 0;
                }
                sysIdleAccumulate += accumulate;
                if (sysIdleAccumulate > SYSTEMIDLE_TIME) {

                    global_GameState = CFGamePlayState_Idle;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////
            } else if (global_GameState == CFGamePlayState_Idle) {
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                idle_c += accumulate;
                if (idle_c > debuginfo_delay) {
                    cout << "Idel... Numof client is " << clientMap.size() << endl;
                    idle_c = 0;
                    
                    //cout << " S = remain client info ======= c " << clientInfo.size() <<  endl;
                    map<string, CFClientInfo>::iterator itr;
                    for (itr = clientInfo.begin(); itr != clientInfo.end(); itr++) {
                        cout << itr->first << endl;
                    }
                    //cout << " E = remain client info =======" << endl;
                }
                if (clientMap.size() == CLIENT_ACCEPTED_LIMIT) {
                   
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd::CFCmd_Prepare;
                    gmutex.lock();
                    broadCastCmdList.push_back(cmdInfo);
                    gmutex.unlock();
                    cout << "--> Prepare" << endl;
                    
                    //Transit state - Prepare
                    global_GameState = CFGamePlayState_Prepare;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                
            } else if (global_GameState == CFGamePlayState_Prepare) {
            
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                prepare_c += accumulate;
                if (prepare_c > debuginfo_delay) {
                    cout << "Prepare:" << prepareAccumulate << endl;
                    prepare_c = 0;
                }
                prepareAccumulate += accumulate;
                if (prepareAccumulate > PREPARE_TIME) {
                    
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd::CFCmd_CountDown;
                    gmutex.lock();
                    broadCastCmdList.push_back(cmdInfo);
                    gmutex.unlock();
                    cout << "--> CountDown" << endl;
                    
                    //Transit state - CountDown
                    global_GameState = CFGamePlayState_CountDown;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                
            } else if (global_GameState == CFGamePlayState_CountDown) {
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                countdown_c += accumulate;
                if (countdown_c > debuginfo_delay) {
                    cout << "CountDown:" << countdownAccumulate << endl;
                    countdown_c = 0;
                }
                countdownAccumulate += accumulate;
                if (countdownAccumulate >= COUNTDOWN_TIME) {
                    
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd::CFCmd_Playing;
                    gmutex.lock();
                    broadCastCmdList.push_back(cmdInfo);
                    gmutex.unlock();
                    cout << "--> Playing" << endl;
                    
                    //Transit state
                    global_GameState = CFGamePlayState_Playing;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////
            } else if (global_GameState == CFGamePlayState_Playing) {
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                playing_c += accumulate;
                if (playing_c > debuginfo_delay) {
                    cout << "Playing:" << playAccumulate << endl;
                    playing_c = 0;
                }
                playAccumulate += accumulate;
                if (playAccumulate >= PLAYLIMIT_TIME) {
                    
                    //Pack Cmd
                    {
                        CFCmdInfo cmdInfo;
                        cmdInfo.cmd = CFCmd::CFCmd_Ended;
                        gmutex.lock();
                        broadCastCmdList.push_back(cmdInfo);
                        gmutex.unlock();
                        cout << "--> Ended" << endl;
                    }
                    
                    //Pack Cmd
                    {
                        CFCmdInfo cmdInfo;
                        cmdInfo.cmd = CFCmd::CFCmd_GameStatistic;
                        gmutex.lock();
                        httpReportCmdList.push_back(cmdInfo);
                        gmutex.unlock();
                    }
                    
                    //Transit state
                    global_GameState = CFGamePlayState_Ended;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////
            } else if (global_GameState == CFGamePlayState_Ended) {
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                ended_c += accumulate;
                if (ended_c > debuginfo_delay) {
                    cout << "Ended:" << endl;
                    ended_c = 0;
                }
                endAccumulate += accumulate;
                if (endAccumulate > ENDFORKICK_TIME) {
        
                    //*Before we go to CFGamePlayState_EndAndKick, players should have time to see game winning screen and statistic screen *//
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd::CFCmd_GameOver;
                    gmutex.lock();
                    httpReportCmdList.push_back(cmdInfo);
                    gmutex.unlock();
                    
                    //Transit state
                    global_GameState = CFGamePlayState_EndAndKick;
                }

            } else if (global_GameState == CFGamePlayState_EndAndKick) {
                
                //Socket Disconnect
                gmutex.lock();
                map<int, string>::iterator itr;
                for (itr = clientMap.begin(); itr != clientMap.end(); itr++) {
                    
                    int socketfd = itr->first;
                    close(socketfd);
                }
                
                clientMap.clear();
                clientInfo.clear();
                reset();
                gmutex.unlock();

            }
            //-------------

            accumulate = accumulate - GAME_STEP_TIME_MSS;
        }
    }

    
}
