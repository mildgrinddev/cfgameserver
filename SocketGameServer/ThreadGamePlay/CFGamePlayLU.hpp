//
//  CFGamePlayLU.hpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFGamePlayLU_hpp
#define CFGamePlayLU_hpp

#include "CFPlayerMng.hpp"
#include "CFMonsterMng.hpp"

class CFGamePlayLU : public CFTickable {

    map<int, string>& clientMap;
    
    map<string, CFClientInfo>& clientInfo;
    
    vector<CFCmdInfo>& broadCastCmdList;
    
    vector<CFCmdInfo>& clientCmdList;
    
    vector<CFCmdInfo>& httpReportCmdList;
        
    CFPlayerMng *playerMng;
    
    CFMonsterMng *monsterMng;
    
    double sysIdleAccumulate;
    
    double prepareAccumulate;
    
    double countdownAccumulate;
    
    double heartBeatAccumulate;
    
    double playAccumulate;
   
    double endAccumulate;
    
    mutex& gmutex;
    
public:
    CFGamePlayLU(map<int, string>& _clientMap, map<string, CFClientInfo>& _clientInfo, vector<CFCmdInfo>& _broadCastCmdList, vector<CFCmdInfo>& _clientCmdList, vector<CFCmdInfo>& _httpReport, mutex& _mutex);
    
protected:
    void reset() override;
    
public:
    void run() override;
};
#endif
