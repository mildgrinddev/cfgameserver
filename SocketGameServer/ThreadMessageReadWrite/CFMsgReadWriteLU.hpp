//
//  CFMsgReadWriteLU.hpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFMsgReadWriteLU_hpp
#define CFMsgReadWriteLU_hpp

class CFMsgReadWriteLU : public CFTickable {
  
    map<int, string>& clientMap;
    
    map<string, CFClientInfo>& clientInfo;
    
    vector<CFCmdInfo>& broadCastCmdList;
    
    mutex& gmutex;
    
public:
    CFMsgReadWriteLU(map<int, string>& _clientMap, map<string, CFClientInfo>& _clientInfo, vector<CFCmdInfo>& _broadCastCmdList, mutex& _mutex);

protected:
    void interpretCommand(char *buffer, int socketfd);
    
public:
    void run() override;
};

#endif
