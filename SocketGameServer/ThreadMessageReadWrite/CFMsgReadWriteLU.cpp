//
//  CFMsgReadWriteLU.cpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/28.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include "CFMsgReadWriteLU.hpp"

CFMsgReadWriteLU::CFMsgReadWriteLU(map<int, string>& _clientMap, map<string, CFClientInfo>& _clientInfo, vector<CFCmdInfo>& _broadCastCmdList, mutex& _mutex):clientMap(_clientMap), clientInfo(_clientInfo),
broadCastCmdList(_broadCastCmdList), gmutex(_mutex) {
    
    CFTickable::reset();
}

void CFMsgReadWriteLU::run() {
    
    while (true) {
               
        // * -- WHILE STS -- *
        
        steady_clock::time_point now = steady_clock::now();
        duration<double> time_span = duration_cast<duration<double> >(now - start);
        start = now;
        
        // Accumulate
        accumulate += time_span.count();
        
        // Total play time
        playTotal += time_span.count();
            
        //////////////////////////////////////////////////////////////////////
        // SEND
        //////////////////////////////////////////////////////////////////////
        gmutex.lock();
        vector<CFCmdInfo>::iterator itr;
        for (itr = broadCastCmdList.begin(); itr != broadCastCmdList.end(); itr++) {

            CFCmdInfo cmdInfo = (*itr);
            if (cmdInfo.cmd == CFCmd_Prepare || cmdInfo.cmd == CFCmd_CountDown || cmdInfo.cmd == CFCmd_Playing || cmdInfo.cmd == CFCmd_Ended || cmdInfo.cmd == CFCmd_HeartBeat) {
                
                 cout << "///////Send CmdType:" << cmdInfo.cmd << endl;
                
                    //pack
                char buffer[2] = {0};
                int count = 2;
                int len = count - 1;
                
                //1 byte - Len
                memcpy(buffer, &len, sizeof(char));
                
                //1 byte - Cmd
                memcpy(&buffer[1], &cmdInfo.cmd, sizeof(char));
                
                map<int, string>::iterator citr;
                for (citr = clientMap.begin(); citr != clientMap.end(); citr++) {
                    cout << "* Send CmdType " << cmdInfo.cmd << " to " << citr->second << endl;
                    send(citr->first, buffer, count, 0);
                }
            } else if (cmdInfo.cmd == CFCmd_ClientInfo) {
                
                if (cmdInfo.action == CFAction_SyncOneClientInfo) {
                    
                    cout << "S CFAction_ShrInfo" << endl;
                    
                    //Get the client info
                    map<string, CFClientInfo>::iterator citr = clientInfo.find(cmdInfo.strPar01);
                    assert(citr != clientInfo.end());

                    //Pack data
                    CFClientInfo clientInfo = citr->second;
                    //cout << "pos " << clientInfo.pos.x << " " << clientInfo.pos.y << " " << clientInfo.pos.z << endl;
                    cout << "pos-->" << global_Pos[clientInfo.side].x << " " << global_Pos[clientInfo.side].y << " " << global_Pos[clientInfo.side].z << endl;
                    cout << "target pos-->" << global_TargetPos[clientInfo.side].x << " " << global_TargetPos[clientInfo.side].y << " " << global_TargetPos[clientInfo.side].z << endl;
                    
                    char buffer[53] = {0};
                    int count = 53;
                    int len = count - 1;
                    
                    //1 byte  - Len
                    memcpy(buffer, &len, sizeof(char));
                    
                    //1 byte  - Cmd
                    memcpy(&buffer[1], &cmdInfo.cmd, sizeof(char));
                    
                    //1 byte - Action
                    memcpy(&buffer[2], &cmdInfo.action, sizeof(char));
                    
                    //4 bytes - userid
                    memcpy(&buffer[3], &clientInfo.user_id, sizeof(char) * 4);
                    
                    //1 byte  - status
                    memcpy(&buffer[7], &clientInfo.status, sizeof(char));
                        
                    //10 bytes - account
                    memcpy(&buffer[8], cmdInfo.strPar01.c_str(), sizeof(char) * 10);
                    
                    //10 bytes - toward account
                    const char *none = "";
                    memcpy(&buffer[18], none, sizeof(char) * 10);
                        
                    //1 byte  - character_type
                    memcpy(&buffer[28], &clientInfo.character_type, sizeof(char));
                     
                    //4 bytes x
                    int x = global_Pos[clientInfo.side].x * 1000;
                    memcpy(&buffer[29], &x, sizeof(char) * 4);
                        
                    //4 bytes y
                    int y = global_Pos[clientInfo.side].y * 1000;
                    memcpy(&buffer[33], &y, sizeof(char) * 4);
                        
                    //4 bytes z
                    int z = global_Pos[clientInfo.side].z * 1000;
                    memcpy(&buffer[37], &z, sizeof(char) * 4);
                    
                    //4 bytes tx
                    int tx = global_TargetPos[clientInfo.side].x  * 1000;
                    memcpy(&buffer[41], &tx, sizeof(char) * 4);
                        
                    //4 bytes ty
                    int ty = global_TargetPos[clientInfo.side].y * 1000;
                    memcpy(&buffer[45], &ty, sizeof(char) * 4);
                        
                    //4 bytes tz
                    int tz = global_TargetPos[clientInfo.side].z * 1000;
                    memcpy(&buffer[49], &tz, sizeof(char) * 4);
                    
                    //////////////////////////////
                    // 1 -> N (include himself)
                    //////////////////////////////
                    map<int, string>::iterator itr;
                    for (itr = clientMap.begin(); itr != clientMap.end(); itr++) {
                        
                        //Send
                        send(itr->first, buffer, count, 0);
                    }
                    //////////////////////////////
                    
                    cout << "E CFAction_ShrInfo" << endl;
                    
                } else if (cmdInfo.action == CFAction_SyncFromAllOthers) {
                   
                    int socketid = -1;
                    map<int, string>::iterator itr;
                    for (itr = clientMap.begin(); itr != clientMap.end(); itr++) {
                        
                        if (itr->second == cmdInfo.strPar01) {
                            socketid = itr->first;
                            break;
                        }
                    }
                    assert(socketid >= 0);

                    //////////////////////////////
                    // N -> 1
                    //////////////////////////////
                    map<string, CFClientInfo>::iterator citr;
                    for (citr = clientInfo.begin(); citr != clientInfo.end() && socketid >= 0; citr++) {
                        
                        if (citr->first != cmdInfo.strPar01) {
                        
                            //Get Info
                            CFClientInfo clientInfo = citr->second;
                            
                            cout << "CFAction_SyncFromAllOthers - send " << citr->first << " to " <<  cmdInfo.strPar01<< endl;
                            
                            //Pack data
                            char buffer[53] = {0};
                            int count = 53;
                            int len = count - 1;
                            
                            //1 byte  - Len
                            memcpy(buffer, &len, sizeof(char));
                            
                            //1 byte  - Cmd
                            memcpy(&buffer[1], &cmdInfo.cmd, sizeof(char));
                            
                            //1 byte  - action
                            memcpy(&buffer[2], &cmdInfo.action, sizeof(char));
                                
                            //4 bytes - userid
                            memcpy(&buffer[3], &clientInfo.user_id, sizeof(char) * 4);
                            
                            //1 byte  - status
                            memcpy(&buffer[7], &clientInfo.status, sizeof(char));

                            //10 bytes - account
                            memcpy(&buffer[8], citr->first.c_str(), sizeof(char) * 10);
                            
                            //10 bytes - toward account
                            const char *none = "";
                            memcpy(&buffer[18], none, sizeof(char) * 10);
                            
                            //1 byte  - character_type
                            memcpy(&buffer[28], &clientInfo.character_type, sizeof(char));
                             
                            //4 bytes x
                            int x = global_Pos[clientInfo.side].z * 1000;
                            memcpy(&buffer[29], &x, sizeof(char) * 4);
                                
                            //4 bytes y
                            int y = global_Pos[clientInfo.side].y * 1000;
                            memcpy(&buffer[33], &y, sizeof(char) * 4);
                                
                            //4 bytes z
                            int z = global_Pos[clientInfo.side].z * 1000;
                            memcpy(&buffer[37], &z, sizeof(char) * 4);
                            
                            //4 bytes tx
                            int tx = global_TargetPos[clientInfo.side].x * 1000;
                            memcpy(&buffer[41], &tx, sizeof(char) * 4);
                                
                            //4 bytes ty
                            int ty = global_TargetPos[clientInfo.side].y * 1000;
                            memcpy(&buffer[45], &ty, sizeof(char) * 4);
                                
                            //4 bytes tz
                            int tz = global_TargetPos[clientInfo.side].z * 1000;
                            memcpy(&buffer[49], &tz, sizeof(char) * 4);
                            
                            //Send
                            send(socketid, buffer, count, 0);
                        }
                    }
                    //////////////////////////////

                } else if (cmdInfo.action == CFAction_Run) {
                    
                    char buffer[37] = {0};
                    int count = 37;
                    int len = count - 1;
                    
                    //1 byte  - Len
                    memcpy(buffer, &len, sizeof(char));
                    
                    //1 byte  - Cmd
                    memcpy(&buffer[1], &cmdInfo.cmd, sizeof(char));
                        
                    //1 byte  - action
                    memcpy(&buffer[2], &cmdInfo.action, sizeof(char));
                        
                    //10 bytes - account
                    memcpy(&buffer[3], cmdInfo.strPar01.c_str(), sizeof(char) * 10);
                    
                    if (clientInfo.find(cmdInfo.strPar01) != clientInfo.end()) {
                                       
                        int side = clientInfo.find(cmdInfo.strPar01)->second.side;
                        
                        int x = global_Pos[side].x * 1000;
                        memcpy(&buffer[13], &x, sizeof(char) * 4);
                        
                        int y = global_Pos[side].y * 1000;
                        memcpy(&buffer[17], &y, sizeof(char) * 4);
                        
                        int z = global_Pos[side].z * 1000;
                        memcpy(&buffer[21], &z, sizeof(char) * 4);
                        
                        int tx = global_TargetPos[side].x * 1000;
                        memcpy(&buffer[25], &tx, sizeof(char) * 4);
                        
                        int ty = global_TargetPos[side].y * 1000;
                        memcpy(&buffer[29], &ty, sizeof(char) * 4);
                        
                        int tz = global_TargetPos[side].z * 1000;
                        memcpy(&buffer[33], &tz, sizeof(char) * 4);
                        
                        map<int, string>::iterator citr;
                        for (citr = clientMap.begin(); citr != clientMap.end(); citr++) {
                                        
                            send(citr->first, buffer, count, 0);
                        }
                        cout << "CFAction_Run X" << global_TargetPos[side].x << " " << global_TargetPos[side].y << " " <<global_TargetPos[side].z << endl;
                    }
                    //////////////////////////////
                } else if (cmdInfo.action == CFAction_Attack01) {
                    
                    //////////////////////////////
                }
                //////////////////////////////////////////////////////////////////////
            } else if (cmdInfo.cmd == CFCmd_ClientDisconnect) {
         
                //////////////////////////////////////////////////////////////////////
            }
        }
        broadCastCmdList.clear();
        gmutex.unlock();
        
        //////////////////////////////////////////////////////////////////////
        // READ
        //////////////////////////////////////////////////////////////////////
        map<int, string>::iterator citr;
        for (citr = clientMap.begin(); citr != clientMap.end(); citr++) {

            // Check socket status by using select
            int socketfd = citr->first;
            fd_set read_sd;
            FD_ZERO(&read_sd);
            FD_SET(socketfd, &read_sd);
            fd_set rsd = read_sd;
            struct timeval waitd = {0, 1};
            int sel = select(socketfd + 1, &rsd, 0, 0, &waitd);
            if (sel > 0) {

                // Buffer
                char ReadBuffer[55] = {0};

                // Some status changed
                ssize_t status = ::recv(socketfd, ReadBuffer, sizeof(ReadBuffer), 0);
                if (status == 0) {

                    printf("disconnection \n");
                    close(socketfd);
                    gmutex.lock();
                    clientMap.erase(citr);
                    gmutex.unlock();
                    
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd_ClientDisconnect;
                    cmdInfo.strPar01 =  citr->second;
                    broadCastCmdList.push_back(cmdInfo);

                    break;
                } else if (status == -1) {

                    printf("some error \n");
                    close(socketfd);
                    gmutex.lock();
                    clientMap.erase(citr);
                    gmutex.unlock();
                    
                    //Pack Cmd
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd_ClientDisconnect;
                    cmdInfo.strPar01 =  citr->second;
                    broadCastCmdList.push_back(cmdInfo);
                    
                    break;
                } else if (status > 0) {
                    
                    printf("status :%zd\n", status);
                    int start = 0;
                    while (true) {
                        char len = ReadBuffer[start];
                        if (len <= 0) {
                            printf("ReadBuffer Error Len:%d\n", len);
                            return;
                        }
                        //assert(len > 0);
                        char buffer[56];
                        memcpy(buffer, &ReadBuffer[start + 1], len);
                        interpretCommand(buffer, socketfd);
                        start += len + 1;
                        if (start >= status) {
                            break;
                        }
                    }
                }
            }
        }
        
        // * -- WHILE END -- *
    }
}

void CFMsgReadWriteLU::interpretCommand(char *buffer, int socketfd) {

    if (sizeof(buffer)) {
           
        // - buffer - sts -

        char type = 0;
        memcpy(&type, buffer, 1);
        CFCmd itype = (CFCmd)type;
        cout << "type:" << itype << endl;
                     
        if (itype == CFCmd::CFCmd_ClientReport) {
                         
                //////////////////////////////////////////////////////////////////////
                // CLIENT_REPORT
                // Look for an existing clientinfo. If not find one, create new one
                //////////////////////////////////////////////////////////////////////
                gmutex.lock();
            
                //04 bytes - userId
                int userid = 0;
                memcpy(&userid, &buffer[1], 4);
                         
                //01 bytes - side
                char side = 0;
                memcpy(&side, &buffer[5], 1);
            
                //01 byte - character_type
                char character_type = 0;
                memcpy(&character_type, &buffer[6], 1);
            
                //10 bytes - account
                char account[10] = {0};
                memcpy(account, &buffer[7], 10);
                string account_str(account);
                cout << account_str << endl;

                //10 bytes - displayname
                char display_name[10] = {0};
                memcpy(display_name, &buffer[17], 10);
                string display_name_str(display_name);
                cout << display_name_str << endl;
                         
                // Set client map
                clientMap[socketfd] = account_str;

                //- ASSIGN VALUE -
                // Look for an existing ClientInfo
                CFClientInfo info;
                cout << "fill init data" << endl;
                info.status = CFClientStatus_Connected;
                info.user_id = userid;
                info.character_type = (CFCharacterType)character_type;
                info.displayname = display_name_str;
                info.side = (CFClientBattleSide)side;
                    
                if (side == 0) {
                    cout << "side:" << side << "Client stand at Right Side. DisplayName:" << display_name_str << " " << account_str << endl;
                } else {
                    cout << "side:" << side << "Client stand at Left Side. DisplayName:" << display_name_str << " " << account_str << endl;
                }
                clientInfo[account_str] = info;
            
                cout << "clinetinfo size " << clientInfo.size() << endl;
                
                // Tell the client about current state 1 -> 1
                CFCmdInfo cmdInfo;
                cmdInfo.cmd = CFCmd_Ended;
                if (global_GameState == CFGamePlayStatus::CFGamePlayState_Prepare) {
                    
                    cout << "reconnect prepare" << endl;
                    cmdInfo.cmd = CFCmd_Prepare;
                } else if (global_GameState == CFGamePlayStatus::CFGamePlayState_CountDown) {
                    
                    cout << "reconnect countdown" << endl;
                    cmdInfo.cmd = CFCmd_CountDown;
                } else if (global_GameState == CFGamePlayStatus::CFGamePlayState_Playing) {
                            
                    cout << "reconnect playing" << endl;
                    cmdInfo.cmd = CFCmd_Playing;
                }
                if (cmdInfo.cmd != CFCmd_Ended) {
                    broadCastCmdList.push_back(cmdInfo);
                }
            
                // Broadcast all client about what the new client is  1 -> N
                {
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd_ClientInfo;
                    cmdInfo.action = CFAction_SyncOneClientInfo;
                    cmdInfo.strPar01 = account_str;
                    broadCastCmdList.push_back(cmdInfo);
                }
            
                // Send all client info to the new client  N -> 1
                {
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd_ClientInfo;
                    cmdInfo.action = CFAction_SyncFromAllOthers;
                    cmdInfo.strPar01 = account_str;
                    broadCastCmdList.push_back(cmdInfo);
                }
                
                gmutex.unlock();
                //////////////////////////////////////////////////////////////////////
            
        } else if (itype == CFCmd::CFCmd_ClientInfo) {
            
            CFAction action = CFAction_Hello;
            memcpy(&action, &buffer[1], 1);
            
            if (action == CFAction_Run) {
                
                gmutex.lock();
                
                //04 bytes - userId
                int userid = 0;
                memcpy(&userid, &buffer[2], 4);
                
                //10 bytes - account
                char account[10] = {0};
                memcpy(account, &buffer[6], 10);
                string account_str(account);
                cout << "account:" << account_str << endl;
                
                //X
                int x = 0;
                memcpy(&x, &buffer[16], 4);
                float fx = x / 1000.0f;
                
                //Y
                int y = 0;
                memcpy(&y, &buffer[20], 4);
                float fy = y / 1000.0f;
                
                //Z
                int z = 0;
                memcpy(&z, &buffer[24], 4);
                float fz = z / 1000.0f;
                
                cout << "CFAction_Run X:" << x << " Y:" << y << " Z:" << z << endl;
                cout << "clinetinfo size " << clientInfo.size() << endl;
                
                //Find the account
                if (clientInfo.find(account_str) != clientInfo.end()) {

                    int side = clientInfo.find(account_str)->second.side;
                    global_TargetPos[side].x = fx;
                    global_TargetPos[side].y = fy;
                    global_TargetPos[side].z = fz;
                        
                    CFCmdInfo cmdInfo;
                    cmdInfo.cmd = CFCmd_ClientInfo;
                    cmdInfo.action = CFAction_Run;
                    cmdInfo.strPar01 = account_str;
                    broadCastCmdList.push_back(cmdInfo);
                }
                gmutex.unlock();
            }
            //////////////////////////////////////////////////////////////////////
        }
        
        
        // - buffer - end -
    }

}
