//
//  CFIOMng.hpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/30.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFIOMng_hpp
#define CFIOMng_hpp

class CFIOMng : public CFTickable {
    
    vector<CFCmd> storageCmdQueue;
    
    vector<CFCmd> databaseCmdQueue;
    
public:
    CFIOMng(vector<CFCmd>& _storageCmdQueue, vector<CFCmd>& _databaseCmdQueue);
    
protected:
    void reset() override;
    
public:
    void run() override;
};
#endif
