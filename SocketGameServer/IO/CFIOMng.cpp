//
//  CFIOMng.cpp
//  SocketGameServer
//
//  Created by chunta on 2020/4/30.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include "CFIOMng.hpp"

CFIOMng::CFIOMng(vector<CFCmd>& _storageCmdQueue, vector<CFCmd>& _databaseCmdQueue):storageCmdQueue(_storageCmdQueue), databaseCmdQueue(_databaseCmdQueue) {
    
}

void CFIOMng::reset() {
    
}
 
void CFIOMng::run() {
    
    while (true) {
        
        steady_clock::time_point now = steady_clock::now();
        duration<double> time_span = duration_cast<duration<double> >(now - start);
        start = now;
        
        // Accumulate
        accumulate += time_span.count();
        
        // Total play time
        playTotal += time_span.count();
        
        // Roughly tick 1 / 60 second
        if (accumulate > GAME_STEP_TIME_MSS) {
            
            accumulate = accumulate - GAME_STEP_TIME_MSS;
        }
    }
}
