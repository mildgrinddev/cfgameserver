
//** Logic Unit **//
#include "CFGamePlayLU.hpp"
#include "CFMsgReadWriteLU.hpp"
#include "CFConnectionListenerLU.hpp"
#include "CFHttpReportLU.hpp"

using namespace std;
using namespace std::chrono_literals;
using namespace std::chrono;

//util
unsigned int random_char();
std::string generate_hex(const unsigned int len);
static string uuid = generate_hex(15);

// server
const char *distributionServer = "http://178.128.52.80:7600/report";

// assign client to a game-room
int thePort = 0;
int theLevel = 0;

// mutex lock
mutex gmutex;

// Socket map
map<int, string> clientMap;

// Client info map
map<string, CFClientInfo> clientInfo;

// Command queue for broadcasting to all clients
vector<CFCmdInfo> cmdQueueBroadCast;

// Command queue for saving, replaying in database
vector<CFCmdInfo> cmdQueueHttpReport;

// Command queue - from client
vector<CFCmdInfo> cmdQueueClient;

// Command queue - save data in database
vector<CFCmdInfo> cmdQueueDatabase;

// Command queue - logic-unit talk to each others
vector<CFCmdInfo> cmdQueueInternal;

CFGamePlayLU *gamePlayLU = 0;
void* thdGameplay(void* data) {

    gamePlayLU = new CFGamePlayLU(clientMap, clientInfo, cmdQueueBroadCast, cmdQueueClient, cmdQueueHttpReport, gmutex);
    gamePlayLU->run();
    return 0;
}

CFMsgReadWriteLU *msgLU = 0;
void* thdMsgReadWrite(void* data) {
    
    msgLU = new CFMsgReadWriteLU(clientMap, clientInfo, cmdQueueBroadCast, gmutex);
    msgLU->run();
    return 0;
}

CFConnectionListenerLU *connectLU = 0;
void* thdConnectionListener(void* data) {
    
    connectLU = new CFConnectionListenerLU(clientMap, thePort);
    connectLU->run();
    return 0;
}

CFHttpReportLU *reportLU = 0;
void* thdReportStatus(void* data) {
    
    reportLU = new CFHttpReportLU(uuid, thePort, theLevel, cmdQueueHttpReport, gmutex);
    reportLU->run();
    return 0;
}

void* thdIO(void* data) {
 
    while (true) {
        
    }
    return 0;
}

int main(int argc , char *argv[]) {

    cout << uuid << endl;
    if (argc != 3) {
        std::cout << "need 3 injected parameters" << "\n";
        return 0;
    }

    std::cout << "argv[1]/level:" << argv[1] << "\n";
    std::cout << "argv[2]/port:"  << argv[2] << "\n";

    char *clvl = argv[1];
    char *cport = argv[2];
    theLevel = atoi(clvl);
    thePort = atoi(cport);

    //Thread
    pthread_t gameplayth;
    pthread_create(&gameplayth, NULL, thdGameplay, 0);
    
    pthread_t msgreadwriteth;
    pthread_create(&msgreadwriteth, NULL, thdMsgReadWrite, 0);
    
    pthread_t conlistenerth;
    pthread_create(&conlistenerth, NULL, thdConnectionListener, 0);
    
    pthread_t reportth;
    pthread_create(&reportth, NULL, thdReportStatus, 0);
    
    pthread_t ioth;
    pthread_create(&ioth, NULL, thdIO, 0);
    
    while(1) {
    
    }
    return 0;
}

unsigned int random_char() {
    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 255);
    return dis(gen);
}

std::string generate_hex(const unsigned int len) {
    std::stringstream ss;
    for (auto i = 0; i < len; i++) {
        const auto rc = random_char();
        std::stringstream hexstream;
        hexstream << std::hex << rc;
        auto hex = hexstream.str();
        ss << (hex.length() < 2 ? '0' + hex : hex);
    }
    return ss.str();
}
