//
//  CFPlayer.hpp
//  SocketGameServer
//
//  Created by apple on 2020/4/21.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFPlayer_hpp
#define CFPlayer_hpp

using namespace std;

class CFPlayer : public CFTickable {
  
    string account;
    
public:
    
    CFPlayer(string _account);
    
};
#endif
