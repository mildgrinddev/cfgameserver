//
//  CFPlayerMng.hpp
//  SocketGameServer
//
//  Created by apple on 2020/4/21.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef CFPlayerMng_hpp
#define CFPlayerMng_hpp

#include "CFPlayer.hpp"

class CFPlayerMng : public CFTickable {
    
    map<int, CFPlayer*> player_map;
    
public:
    
    CFPlayerMng();

    void addPlayer(int _socketId);
    
    void removePlayer(int _socketId);
    
    void dealCmdFromPlayer(int _socketId, int cmd);
    
public:
    
    void run() override;
    
};
#endif
