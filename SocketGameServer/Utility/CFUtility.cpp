//
//  utility.hpp
//  SocketGameServer
//
//  Created by Chun Ta Chen on 2019/9/29.
//  Copyright © 2019 Chun Ta Chen. All rights reserved.
//


#include "CFUtility.hpp"
#include <stdio.h>
#include <random>
using namespace std;

uint64_t timeSinceEpochMillisec()
{
    return chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
}

string randomString()
{
    string str = "AAAAAA";
    
    std::random_device rd;
    std::default_random_engine gen = std::default_random_engine(rd());
    std::uniform_int_distribution<int> dis(1, 100000000);

    
    // string sequence
    str[0] = dis(gen) % 26 + 65;
    str[1] = dis(gen) % 26 + 65;
    str[2] = dis(gen) % 26 + 65;
    
    // number sequence
    str[3] = dis(gen) % 10 + 48;
    str[4] = dis(gen) % 10 + 48;
    str[5] = dis(gen) % 10 + 48;
    
    return str;
}

void tokenize(std::string const &str, const char delim,
              std::vector<std::string> &out)
{
    size_t start;
    size_t end = 0;
    
    while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}
