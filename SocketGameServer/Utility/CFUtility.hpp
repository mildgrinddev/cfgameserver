//

//  utility.h
//  SocketGameServer
//
//  Created by Chun Ta Chen on 2019/9/29.
//  Copyright © 2019 Chun Ta Chen. All rights reserved.
//
// Please use 'Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-59780228'
#ifndef utility_h
#define utility_h
#include <chrono>
#include <vector>
#include <iostream>
#include <string>

using namespace std;

uint64_t timeSinceEpochMillisec();
string randomString();
void tokenize(std::string const &str, const char delim, std::vector<std::string> &out);

template<typename T>
vector<T> slice(vector<T> &v, int start, int len)
{
    auto first = v.cbegin() + start;
    auto last = first + len;
    
    vector<T> vec(first, last);
    
    v.erase(first, last);
    
    return vec;
}

#endif /* utility_h */
