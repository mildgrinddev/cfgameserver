//
//  CFModel.cpp
//  SocketGameServer
//
//  Created by apple on 2020/5/24.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#include <stdio.h>
#include "CFModel.hpp"
CFVector   global_Pos[2] = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
CFVector  global_TargetPos[2] = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
float      global_Rot[2] = { 90, -90};
float    global_Speed[2] = { 1.0f, 1.0f};

CFVector   global_InitPos[2] = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
CFVector  global_InitTargetPos[2] = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
float      global_InitRot[2] = { 90, -90};
float    global_InitSpeed[2] = { 1.0f, 1.0f};

CFGamePlayStatus global_GameState = CFGamePlayStatus::CFGamePlayState_SystemIdle;
