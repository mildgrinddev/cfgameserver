//
//  model.h
//  SocketGameServer
//
//  Created by apple on 2020/2/1.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef model_h
#define model_h

#include <stdio.h>
#include <map>
#include <iostream>
#include <vector>
#include <cmath>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <vector>
#include <map>
#include <atomic>
#include <fcntl.h>
#include <pthread.h>
#include <chrono>
#include <ctime>
#include <mutex>
#include <exception>
#include <sys/types.h>
#include <thread>
#include <iostream>
#include <ratio>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <algorithm>
#include <arpa/inet.h>
#include <fstream>
#include <sstream>
#include <random>
#include "CFUtility.hpp"
#include <cassert>

//namespace
using namespace std;

const static float GAME_STEP_TIME_MSS    = (1000.0 / 60.0) / 1000.0;
const static float GAME_ACCUMULATE       = 0.0f;
const static float GAME_PREV             = 0.0f;
const static float MONSTER_DETECT_DELAY  = 1.0f;
const static float MONSTER_ATTACK_DELAY  = 1.0f;
const static float MONSTER_CANCELTARGET_DELAY = 20.0f;
const static float MONSTER_DETECT_MOVE   = 700;
const static float MONSTER_DETECT_ATTK   = 500;
const static float MONSTER_DETECT_ATTK_FALLBACK  = 800;
const static   int CLIENT_ACCEPTED_LIMIT = 2;
const static float SYSTEMIDLE_TIME       = 2;
const static float PREPARE_TIME          = 4;
const static float COUNTDOWN_TIME        = 4;
const static float PLAYLIMIT_TIME        = 60 * 2; // 65 secs - one game period
const static float HEARTBEAT_TIME        = 10;
const static float ENDFORKICK_TIME       = 5;
const static float HTTPREQUEST_REPORT_TIME = 1;
const static   int DEFAULT_SIZEOF_BUFFER = 30;
const static char* CMD_TEST    = "Test";
const static char* CMD_NEWCONN = "NewConnect";
const static char* CMD_LEVEL   = "NewLevel";
const static char* CMD_MONSTER = "Monster";
const static char* CMD_DISCONN = "DisConnect";
const static char* CMD_HURT_MONSTER = "Hurt_Monster";
const static char* CMD_POSE_PLAYER  = "Pose_Player";
const static char* CMD_MOV_PLAYER   = "Mov_Player";
const static char* CMD_REPORT_LOC_PLAYER  = "ReportLoc_Player";
const static char* CMD_REPORT_LOC_MONSTER = "ReportLoc_Monster";
const static char* CMD_SERVER_MOV_MONSTER_TO_ONECLIENT        = "Server_MonsterMoveToClient";
const static char* CMD_SERVER_MOV_MONSTER_TO_ONECLIENT_ATTK   = "Server_MonsterMoveToClient_Attk";
const static char* CMD_SERVER_MOV_MONSTER_TO_ONECLIENT_CANCEL = "Server_MonsterMoveToClientCancel";

const static char* DISTRIBUT_SERVER_REPORT = "http://178.128.52.80:7600/report";
const static char* DISTRIBUT_SERVER_GAMEOVER = "http://178.128.52.80:7600/gameover";
const static char* STATISTIC_SERVER = "http://127.0.0.1:7600/report";

struct CFVector2D {

    int x;
    int y;

    CFVector2D(int _x, int _y) {

        x = _x;
        y = _y;
    }
};

typedef struct CFVector {

    int x;
    int y;
    int z;

    CFVector(int _x, int _y, int _z) {

        x = _x;
        y = _y;
        z = _z;
    }

    CFVector() {

        x = 0;
        y = 0;
        z = 0;
    }
    
    CFVector(const CFVector& a) : x(a.x), y(a.y), z(a.z) { } // user-defined copy ctor

    int HorizontalDistanceBetween(CFVector another) {

        int dx = x - another.x;
        int dy = y - another.y;
        int dz = 0;
        return sqrt(dx*dx + dy*dy + dz*dz);
    }

    int DistanceBetween(CFVector another) {

        int dx = x - another.x;
        int dy = y - another.y;
        int dz = z - another.z;
        return sqrt(dx*dx + dy*dy + dz*dz);
    }

} CFVector;

typedef enum {

    CFGamePlayState_SystemIdle = 0,

    CFGamePlayState_Idle,

    CFGamePlayState_Prepare,

    CFGamePlayState_CountDown,

    CFGamePlayState_Playing,

    CFGamePlayState_Ended,

    CFGamePlayState_EndAndKick

} CFGamePlayStatus;

typedef enum {

    CFMonsterStatus_Idle = 0,

    CFMonsterStatus_Moving,

    CFMonsterStatus_Attack,

    CFMonsterStatus_Hurt

} CFMonsterStatus;

typedef enum {
    
    CFAction_Hello    = 0,
    
    CFAction_Attack01 = 1,
    
    CFAction_Attack02 = 2,
    
    CFAction_Attack03 = 3,
    
    CFAction_Attack04 = 4,
    
    CFAction_Attack05 = 5,
    
    CFAction_Attack06 = 6,
    
    CFAction_Attack07 = 7,
    
    CFAction_Attack08 = 8,
    
    CFAction_Attack09 = 9,
    
    CFAction_Attack10 = 10,
    
    CFAction_Attack11 = 11,
    
    CFAction_Attack12 = 12,
    
    CFAction_Attack13 = 13,
    
    CFAction_Attack14 = 14,
    
    CFAction_Attack15 = 15,
    
    CFAction_Attack16 = 16,
    
    CFAction_Buff0001 = 17,
    
    CFAction_Buff0002 = 18,
    
    CFAction_Buff0003 = 19,
    
    CFAction_Buff0004 = 20,
    
    CFAction_Buff0005 = 21,
    
    CFAction_Buff0006 = 22,
    
    CFAction_Buff0007 = 23,
    
    CFAction_Buff0008 = 24,
    
    CFAction_Buff0009 = 25,
    
    CFAction_Buff0010 = 26,
    
    CFAction_Buff0011 = 27,
    
    CFAction_Buff0012 = 28,
    
    CFAction_Buff0013 = 29,
    
    CFAction_Buff0014 = 30,
    
    CFAction_Buff0015 = 31,
    
    CFAction_Buff0016 = 32,
    
    CFAction_Run      = 33,
    
    CFAction_Idle     = 34,
    
    CFAction_SyncOneClientInfo  = 35, //Share one client info to others
    
    CFAction_SyncFromAllOthers  = 36  //Sync all other clients and monster info to one client
    
} CFAction;

typedef enum {

    //One client upload their profile, like what character they chose
    CFCmd_ClientReport = 0,

    //One client sisconnect
    CFCmd_ClientDisconnect = 1,

    //Clients should be already in the play area. Tell them to prepare, like change rune or skills
    CFCmd_Prepare = 2,

    //Players should alrady set up all they need, then server annouces count-down 30 secs
    CFCmd_CountDown = 3,

    //The game has started
    CFCmd_Playing = 4,

    //The game has ended
    CFCmd_Ended = 5,

    //Heart beat
    CFCmd_HeartBeat = 6,

    //Show Game Statistics calculated from a statistic server
    CFCmd_GameStatistic = 7,
    
    //The game has ended.
    CFCmd_GameOver = 8,
    
    //BroadCast One client information to all clients
    CFCmd_ClientInfo = 9,
    
    //BroadCast one monster information to all clients
    CFCmd_MonsterInfo = 10

} CFCmd;

typedef enum {

    CFClientStatus_Unreport = 0,

    CFClientStatus_Connected = 1,

    CFClientStatus_Disconnected = 2

} CFClientStatus;

typedef enum {

    CFClientBattleSide_Left = 0,
    
    CFClientBattleSide_Right = 1,

} CFClientBattleSide;

typedef enum {

    CFCharacterType_White = 0,
    
    CFCharacterType_DarkRed = 1,
    
    CFCharacterType_Frog = 2,
    
    CFCharacterType_Orange = 3,

} CFCharacterType;

struct CFCmdInfo {

    CFCmd cmd;
    
    CFAction action;

    string strPar01;
    
    int intPar01;
};

//   0 - Left  1 - Right
extern CFVector   global_Pos[2];
extern CFVector  global_TargetPos[2];// = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
extern float      global_Rot[2];// = { 90, -90};
extern float    global_Speed[2];// = { 1.0f, 1.0f};

extern CFVector   global_InitPos[2];// = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
extern CFVector  global_InitTargetPos[2];// = { CFVector(0, -4640, 100), CFVector(0, 4640, 100)};
extern float      global_InitRot[2];// = { 90, -90};
extern float    global_InitSpeed[2];// = { 1.0f, 1.0f};

extern CFGamePlayStatus global_GameState;

struct CFClientInfo {

    int user_id;
    
    CFCharacterType character_type;
    
    CFClientBattleSide side;
    
    string displayname;

    CFClientStatus status;

    uint64_t lasttime_active;

    //CFVector pos;
    
    //CFVector target_pos;
    
    //float rot;
    
    //float speed;

    CFClientInfo():user_id(-1), character_type(CFCharacterType_White), side(CFClientBattleSide_Left), displayname(""), status(CFClientStatus_Unreport), lasttime_active(0) {

    }
};

struct CFMonsterInfo {

    int type;

    float timeAccumulated_Detection;

    float timeAccumulated_Attack;

    float timeAccumulated_CancelTarget;

    CFMonsterStatus status;

    CFVector loc;

    float yaw;

    bool hasTarget;

    string targetAccount;

    CFMonsterInfo(int _type, CFVector _loc) {

        type = _type;
        timeAccumulated_Detection = 0;
        timeAccumulated_CancelTarget = 0;
        status = CFMonsterStatus_Idle;
        loc = _loc;
        hasTarget = false;
    }

    CFMonsterInfo() {
        type = 0;
    }
};

typedef enum
{
    GAMESTATUS_WAIT_CLIENT = 0,
    GAMESTATUS_COUNTDOWN = 1,
    GAMESTATUS_PLAYING = 2,
    GAMESTATUS_LESS_1_MIN = 3,
    GAMESTATUS_LESS_6_MIN = 4,
    GAMESTATUS_LESS_12_MIN = 5,
    GAMESTATUS_END = 6
} GameStatus;

static const float GAMESTATUS_LAST_TABLE[6] = {4.0f, 10.0f, 0.0f, 60.0f, 60 * 5.0f, 60 * 6.0f};

#endif
