//
//  PrefixHeader.pch
//  SocketGameServer
//
//  Created by apple on 2020/5/4.
//  Copyright © 2020 Chun Ta Chen. All rights reserved.
//

#ifndef Prefix_pch
#define Prefix_pch

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

//CF
#include "CFModel.hpp"
#include "CFTickable.hpp"


#endif
